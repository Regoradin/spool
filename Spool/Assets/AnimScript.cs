using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AnimScript : MonoBehaviour
{
    private Animator animator;

    private bool isRunning = true;
    
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        
    }

    void OnMove(InputValue input) 
    {
        if (isRunning) {
            animator.SetBool("isRunning", true);
            isRunning = false;
        }
        else {
            animator.SetBool("isRunning", false);
            isRunning = true;
        }
        
    }
}
