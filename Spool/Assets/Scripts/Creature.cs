using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature
{
    private string name;
    private bool isPlayer;
    
    private PlayerMover playerMover;
    public MazeNode currentMazeNode { get; private set; }

    private Strand currentStrand;

    public Creature(string name, int startingX, int startingY, bool isPlayer, Strand strand = null, PlayerMover mover = null)
    {
        this.name = name;
        this.isPlayer = isPlayer;
        this.currentStrand = strand;
        this.playerMover = mover;
        MazeNode startingNode = LogicalMaze.getMazeNode(startingX, startingY);
        if (startingNode == null)
        {
            Debug.LogError(name + " started in an invalid position: " + startingX + " " + startingY);
            return;
        }
        EnterNode(LogicalMaze.getMazeNode(startingX, startingY));
    }

    public override string ToString()
    {
        return name;
    }

    public Vector2Int GetCurrentPosition()
    {
        return new Vector2Int(currentMazeNode.xCoord, currentMazeNode.yCoord);
    }

    public MazeNode[] GetAvailableRooms()
    {
        // returned in NESW order
        return currentMazeNode.GetConnectedNodes();
    }

    public bool TryMove(Vector2Int direction, float duration = -1)
    {
        MazeNode nextNode = LogicalMaze.getMazeNode(currentMazeNode.xCoord + direction.x,
                                                    currentMazeNode.yCoord + direction.y);        
        if (!IsMovementDirectionValid(direction)
            || nextNode == null // this isn't necessarily an error, it will also happen when hitting a wall
            || !LogicalMaze.IsConnected(currentMazeNode, nextNode))
        {
            return false;
        }

        if (isPlayer && !Spool.isDropped) 
        {
            DropThread(currentMazeNode, nextNode, direction);   
        }
        
        EnterNode(nextNode, duration);
        CheckForDefeat();
        
        return true;
    }

    public void InteractWithMazeNode()
    {
        if (currentMazeNode.IsDoor())
        {
            RunManager.GetInstance().EndRun(victorious: true, removeThread: false);
        }
        else
        {
            if (!Spool.isDropped)
            {
                Spool.Drop(currentMazeNode);
            }
            else if (Spool.node == currentMazeNode)
            {
                Spool.PickUp();
            }
        }
    }

    private void DropThread(MazeNode curr, MazeNode entering, Vector2Int direction)
    {
        if (direction.x == 1)
        {
            entering.SetThreadWest(currentStrand);
            curr.SetThreadEast(currentStrand);
        }
        if (direction.x == -1)
        {
            entering.SetThreadEast(currentStrand);
            curr.SetThreadWest(currentStrand);
        }
        if (direction.y == 1)
        {
            entering.SetThreadNorth(currentStrand);
            curr.SetThreadSouth(currentStrand);
        }
        if (direction.y == -1)
        {
            entering.SetThreadSouth(currentStrand);
            curr.SetThreadNorth(currentStrand);
        }

        DebugMazeViewer.GetInstance().DrawAllThreadsForMazeNode(curr);
        DebugMazeViewer.GetInstance().DrawAllThreadsForMazeNode(entering);
    }

    private void CheckForDefeat()
    {
        if (currentMazeNode.GetCurrentCreatures().Count > 1)
        {
            if (!Spool.isDropped) {
                RunManager.GetInstance().EndRun(victorious: false, removeThread: true);
            }
            else
            {
                RunManager.GetInstance().EndRun(victorious: false, removeThread: false);
            }
        }
    }

    private bool IsMovementDirectionValid(Vector2Int direction)
    {
        if (direction.x == direction.y)
        {
            Debug.LogWarning("Invalid move order given to " + name + ": " + direction);
            return false;
        }
        if (Mathf.Abs(direction.x) > 1 || Mathf.Abs(direction.y) > 1)
        {
            Debug.LogWarning("Large move order given to " + name + ": " + direction);
            return false;
        }
        return true;
    }

    private void EnterNode(MazeNode node, float duration = -1)
    {

        if (currentMazeNode != null)
        {
            currentMazeNode.Exit(this);
        }
        bool hasThread = !Spool.isDropped;
        if (duration != -1)
        {
            playerMover.SetMoveDuration(duration);
        }
        playerMover.MoveSprite(currentMazeNode, node, isPlayer, hasThread);
        node.Enter(this);
        currentMazeNode = node;
        if(isPlayer)
        {
            playerMover.HasMoved();
        }    
    }

    

}
