using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EllerMazeGenerator : MonoBehaviour
{
    [SerializeField] private Vector2Int size;
    [SerializeField, Range(0, 1)] private float horizontalness;
    [SerializeField, Range(0, 3)] private float loopiness;
    private float horizontalCarveChance;
    private float verticalCarveChance;

    [SerializeField] private Vector2Int doorCoords;

    public void GenerateMaze() {
        // Setup some numbers
        horizontalCarveChance = horizontalness;
        verticalCarveChance = 1 - horizontalness;

        
        // Implementation of Eller's Algorithm (https://weblog.jamisbuck.org/2010/12/29/maze-generation-eller-s-algorithm)

        MazeNode[,] maze = new MazeNode[size.x, size.y];

        for(int x = 0; x < size.x; x++)
        {
            for(int y = 0; y < size.y; y++)
            {
                maze[x,y] = new MazeNode(x, y, isDoor: (x == doorCoords.x && y == doorCoords.y));
            }
        }

        // This implementation is O(N^2) I think, where N is the total number of cells
        // Not great, and certainly room for improvment
        // And just awful in terms of memory I think. But we're only doing it a few times so it should be fine

        // "Sets" are ways to track whether nodes can be reached from each other
        // If two nodes are in the same set, they are somehow connected.

        // Assume there are walls everywhere, until we carve paths between cells

        Dictionary<MazeNode, HashSet<MazeNode>> nodeToSets = new Dictionary<MazeNode, HashSet<MazeNode>>();        
        // Set up initial sets in the first row - all disconnected
        for (int x = 0; x < size.x; x++)
        {
            HashSet<MazeNode> newSet = new HashSet<MazeNode>();
            newSet.Add(maze[x, 0]);
            nodeToSets[maze[x, 0]] = newSet;
        }

        for(int y = 0; y < size.y - 1; y++)
        {
            // Carve Horizontally
            for(int x = 0; x < size.x - 1; x++)
            {
                if (nodeToSets[maze[x,y]].Contains(maze[x+1,y]))
                {
                    // would cause a loop
                    if (Random.Range(0f, 1f) < loopiness
                        && Random.Range(0f, 1f) < (horizontalCarveChance + (loopiness * 0.3f))) // boost slightly to encourage loop forming in high-loopiness scenerios
                    {
                        CarveRight(maze[x,y], maze[x+1,y], nodeToSets);
                    }
                }
                else if (Random.Range(0f, 1f) < horizontalCarveChance)
                {
                    // would not cause a loop
                    CarveRight(maze[x,y], maze[x+1,y], nodeToSets);
                }
            }
            // Carve Vertically
            HashSet<MazeNode> nodesCarvedDown = new HashSet<MazeNode>();
            for(int x = 0; x < size.x; x++)
            {
                if (Random.Range(0f, 1f) < verticalCarveChance
                    || nodeToSets[maze[x,y]].Count == 1) // no isolates!
                {
                    CarveDown(maze[x,y], maze[x,y+1], nodeToSets);
                    nodesCarvedDown.Add(maze[x,y]);
                }
                else
                {
                    //if we're not connected from above, we're not connected anywhere
                    HashSet<MazeNode> newSet = new HashSet<MazeNode>();
                    newSet.Add(maze[x, y+1]);
                    nodeToSets[maze[x, y+1]] = newSet;
                }
            }            
            //sweep through the row again to ensure that every set is connected down somewhere
            for(int x = 0; x < size.x; x++)
            {
                //only do an extra carve if this node's set hasn't been carved down yet
                //almost certainly slow and bad
                if (nodeToSets[maze[x,y]].Intersect(nodesCarvedDown).Count() == 0)
                {
                    CarveDown(maze[x,y], maze[x,y+1], nodeToSets);
                    nodesCarvedDown.Add(maze[x,y]);
                }
            }

        }

        //wrap things up in the last row
        for(int x = 0; x < size.x - 1; x++)
        {
            if (!nodeToSets[maze[x,size.y-1]].Contains(maze[x+1, size.y-1]))
            {
                // connect anything from different sets, so we get one big set on the bottom
                // which means that the whole maze is all connected
                CarveRight(maze[x,size.y-1], maze[x+1,size.y-1], nodeToSets);
            }
        }


        //Incorporate old maze:
        MazeNode[,] oldMaze = LogicalMaze.getMazeArray();

        if (oldMaze != null)
        {

            //Copy thread state
            for(int x = 0; x < size.x; x++)
            {
                for(int y = 0; y < size.y; y++)
                {
                    if (oldMaze[x,y].IsThreadNorth()) maze[x,y].SetThreadNorth(oldMaze[x,y].GetThreadsNorth().Peek());
                    if (oldMaze[x,y].IsThreadSouth()) maze[x,y].SetThreadSouth(oldMaze[x,y].GetThreadsSouth().Peek());
                    if (oldMaze[x,y].IsThreadEast()) maze[x,y].SetThreadEast(oldMaze[x,y].GetThreadsEast().Peek());
                    if (oldMaze[x,y].IsThreadWest()) maze[x,y].SetThreadWest(oldMaze[x,y].GetThreadsWest().Peek());

                }

            }
            
            //Copy connection state
            for(int x = 0; x < size.x; x++)
            {
                for(int y = 0; y < size.y; y++)
                {
                    try
                    {
                        if (oldMaze[x,y].HasAnyThread())
                        {
                            if (oldMaze[x,y].IsThreadNorth())
                            {
                                maze[x,y].Connect(north: maze[x, y-1]);
                                maze[x, y-1].Connect(south: maze[x,y]);
                            }
                            if (oldMaze[x,y].IsThreadEast())
                            {
                                maze[x,y].Connect(east: maze[x+1, y]);
                                maze[x+1, y].Connect(west: maze[x,y]);
                            }
                            if (oldMaze[x,y].IsThreadSouth())
                            {
                                maze[x,y].Connect(south: maze[x, y+1]);
                                maze[x, y+1].Connect(north: maze[x,y]);
                            }
                            if (oldMaze[x,y].IsThreadWest())
                            {
                                maze[x,y].Connect(west: maze[x-1, y]);
                                maze[x-1, y].Connect(east: maze[x,y]);
                            }
                        }                        
                    }
                    catch
                    {
                        Debug.LogWarning("Something went funky while copying threads");
                    }
                }
            }
        }

        LogicalMaze.CreateMaze(maze, maze[doorCoords.x, doorCoords.y]);
    }

    private void CarveRight(MazeNode left, MazeNode right, Dictionary<MazeNode, HashSet<MazeNode>> nodeToSets)
    {
        //assuming that these are actually adjacent and whatnot, or else this will get strange
        left.Connect(east: right);
        right.Connect(west: left);

        //join the two sets:
        //make the left set eat the right set
        nodeToSets[left].UnionWith(nodeToSets[right]);
        //make every node in the right set use the left set
        foreach(MazeNode node in nodeToSets[right])
        {
            nodeToSets[node] = nodeToSets[left];
        }
    }

    private void CarveDown(MazeNode top, MazeNode bottom, Dictionary<MazeNode, HashSet<MazeNode>> nodeToSets)
    {
        //assuming that these are actually adjacent and whatnot, or else this will get strange
        top.Connect(south: bottom);
        bottom.Connect(north: top);

        nodeToSets[top].Add(bottom);
        nodeToSets[bottom] = nodeToSets[top];
    }
}
