using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DebugCreatureViewer))]
[RequireComponent(typeof(PlayerInputManager))]
public class Player : MonoBehaviour
{
    // This class should almost entirely be for orchestration and wiring up other scripts
    // Specific logic should have its own home
    
    [SerializeField] private PlayerMover mover;
    public Creature creature { get; private set; }
    public Strand strand {get; private set;}

    private void Awake()
    {
        MazeNode door = LogicalMaze.getDoorNode();
        strand = new Strand(1, Color.yellow);
        creature = new Creature("DebugPlayer", door.xCoord, door.yCoord, true, strand, mover);
        GetComponent<DebugCreatureViewer>().SetCreature(creature);
        GetComponent<PlayerInputManager>().Creature = creature;
    }
}
