using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreadBrain : IBrain
{
    private HashSet<MazeNode> exploredMazeNodes;

    public ThreadBrain()
    {
        exploredMazeNodes = new HashSet<MazeNode>();
    }

    public BrainResponse GetBrainResponse(MazeNode currentNode, Creature target = null)
    {
        BrainResponse response = new BrainResponse();
        response.speed = MinotaurSpeeds.threadSpeed;

        response.nextNode = NextNodeOnThread(currentNode);

        response.nextBrain = ChooseNextBrain(currentNode, target);

        return response;
    }

    private IBrain ChooseNextBrain(MazeNode currentNode, Creature target)
    {
        if (target != null && NavigationUtils.HasLineOfSight(currentNode, target.currentMazeNode))
        {
            ResetMemory();
            return BrainBank.chargeBrain;
        }
        else if (HasUnexploredThreads(currentNode))
        {
            return this;
        }
        else 
        {
            ResetMemory();
            return BrainBank.wanderingBrain;
        }
    }

    private void ResetMemory()
    {
        return;
        //Question: do we want to reset the explored nodes every time you switch states?
        // exploredMazeNodes = new HashSet<MazeNode>();
    }

    private MazeNode NextNodeOnThread(MazeNode node)
    {
        exploredMazeNodes.Add(node);

        if(!HasUnexploredThreads(node))
        {
            return null;
        }

        List<MazeNode> adjacentMazeNodesToExplore = new List<MazeNode>();
        for(int i = 0; i < node.GetConnectedNodes().Length; i++)
        {
            MazeNode adjacentNode = node.GetConnectedNodes()[i];
            if (adjacentNode != null && node.GetThreadArray()[i] && !exploredMazeNodes.Contains(adjacentNode))
            {
                adjacentMazeNodesToExplore.Add(adjacentNode);
            }
        }

        return adjacentMazeNodesToExplore[Random.Range(0, adjacentMazeNodesToExplore.Count)];
    }

    private bool HasUnexploredThreads(MazeNode node)
    {
        //check if any of these threads lead in a direction that hasn't been explored
        for(int i = 0; i < node.GetConnectedNodes().Length; i++)
        {
            MazeNode adjacentNode = node.GetConnectedNodes()[i];
            if (adjacentNode != null && node.GetThreadArray()[i] && !exploredMazeNodes.Contains(adjacentNode))
            {
                return true;
            }
        }

        return false;
    }
    
    public string GetStateString()
    {
        string debugString = "State: Following Thread";
        debugString += "\n\nExploredNodes:";
        foreach(MazeNode node in exploredMazeNodes)
        {
            debugString += node.xCoord + " " + node.yCoord + "\n";
        }
        return debugString;
    }
}
