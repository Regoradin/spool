using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BrainBank
{
    // this should probably be scoped down to be minotaur specific and not static
    // can't be bothered at the moment though

    public static WanderingBrain wanderingBrain = new WanderingBrain();
    public static ChargeBrain chargeBrain = new ChargeBrain();
    public static ThreadBrain threadBrain = new ThreadBrain();
}
