using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugMazeViewer : MonoBehaviour
{
    private static DebugMazeViewer instance;
    public static DebugMazeViewer GetInstance()
    {
        if (instance == null)
        {
            DebugMazeViewer.instance =  new GameObject().AddComponent<DebugMazeViewer>();
        }
        return instance ;
    }
    private GameObject mazeObject;

    public Sprite floorSprite;
    public Sprite doorSprite;
    public Sprite[] wallSprites;
    public Sprite[] newThreadSprites;
    public Sprite[] oldThreadSprites;
    public GameObject newThreadParticles;
    public GameObject oldThreadParticles;

    //TODO: come up with something more efficient here
    private Dictionary<MazeNode, List<GameObject>> mazeNodeToThreads;

    private void Awake()
    {
        instance = this;
        
        mazeObject = new GameObject();
        mazeObject.name = "Maze";
        mazeObject.transform.SetParent(transform);

        mazeNodeToThreads = new Dictionary<MazeNode, List<GameObject>>();

    }
            
    public void DrawMaze() // Generates the debug view, clearing everything first
    {
        // Magic numbers galore! Very temp, just to get some visuals
        MazeNode[,] maze = LogicalMaze.getMazeArray();

        // Clear previous maze
        foreach(Transform child in mazeObject.transform)
        {
            Destroy(child.gameObject);
        }
        
        for (int x = 0; x < maze.GetLength(0); x++)
        {
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                if (maze[x, y] != null)
                {
                    GameObject room = new GameObject();
                    room.transform.position = GetWorldPosition(x, y);
                    room.name = "Room " + x + " " + y;
                    room.transform.SetParent(mazeObject.transform);

                    // create this on a child, so that our rotations don't break anything else
                    GameObject roomSprite = new GameObject();
                    roomSprite.transform.SetParent(room.transform);
                    roomSprite.transform.localPosition = Vector3.zero;
                    roomSprite.transform.Rotate(Vector3.right * 90f);

                    SpriteRenderer renderer = roomSprite.AddComponent<SpriteRenderer>();
                    renderer.sprite = floorSprite;

                    if (maze[x,y].IsDoor())
                    {
                        GameObject door = new GameObject();
                        door.transform.SetParent(room.transform);
                        door.transform.localPosition = Vector3.up * 1f;
                        door.name = "Door";
                        door.transform.Rotate(Vector3.right * 90f);
                        SpriteRenderer wallRenderer = door.AddComponent<SpriteRenderer>();
                        wallRenderer.sprite = doorSprite;
                    }

                    MazeNode[] connectedNodes = maze[x,y].GetConnectedNodes();
                    for (int r = 0; r < connectedNodes.Length; r++)
                    {
                        if (connectedNodes[r] == null)
                        {
                            GameObject hall = new GameObject();
                            hall.transform.SetParent(room.transform);
                            hall.transform.localPosition = Vector3.up * 1f;
                            hall.name = "Connection";
                            hall.transform.Rotate(Vector3.right * 90f);
                            SpriteRenderer wallRenderer = hall.AddComponent<SpriteRenderer>();
                            wallRenderer.sprite = wallSprites[r];
                        }
                    }

                    DrawAllThreadsForMazeNode(maze[x,y]);
                }
            }
        }
    }

    public void DrawAllThreadsForMazeNode(MazeNode node)
    {
        // clean up old threads
        if (mazeNodeToThreads.ContainsKey(node))
        {
            foreach(GameObject oldThread in mazeNodeToThreads[node])
            {
                Destroy(oldThread);
            }
        }
        List<GameObject> newThreads = new List<GameObject>();

        if (node.IsThreadNorth()) {
            newThreads.Add(DrawThread(node.xCoord, node.yCoord, 0f, node.GetThreadsNorth().Peek().getColor()));
        }
        if (node.IsThreadSouth()) {
            newThreads.Add(DrawThread(node.xCoord, node.yCoord, 180f, node.GetThreadsSouth().Peek().getColor()));
        }
        if (node.IsThreadEast()) {
            newThreads.Add(DrawThread(node.xCoord, node.yCoord, 90f, node.GetThreadsEast().Peek().getColor()));
        }
        if (node.IsThreadWest()) {
            newThreads.Add(DrawThread(node.xCoord, node.yCoord, 270f, node.GetThreadsWest().Peek().getColor()));
        }

        mazeNodeToThreads[node] = newThreads;
    }

    private GameObject DrawThread(int x, int y, float rotation, Color strandColor)
    {
        GameObject thread = new GameObject();
        thread.transform.position = GetWorldPosition(x, y) + (Vector3.up * 2f); 
        thread.transform.SetParent(mazeObject.transform);
        thread.transform.Rotate(Vector3.right * 90f);
        thread.name = "Thread";

        SpriteRenderer renderer = thread.AddComponent<SpriteRenderer>();
        //hacky and bad :)
        bool isNewThread = (strandColor == Color.yellow);
        renderer.sprite = (isNewThread ? newThreadSprites : oldThreadSprites)[(int)(rotation / 90)];
        Transform particles = Instantiate(isNewThread ? newThreadParticles : oldThreadParticles).transform;
        particles.Rotate(Vector3.up * rotation);
        particles.position = thread.transform.position;
        particles.SetParent(thread.transform);
        
        

        return thread;
    }

    
    private Vector3 GetWorldPosition(int x, int y)
    {
        // Converts logical maze x and y into world coords
        //Unity is backwards of what I though, hence the negative y
        return new Vector3(x, 0, -y);
    }
}
