using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunManager : MonoBehaviour
{
    public static RunManager instance;

    public static RunManager GetInstance()
    {
        if (!instance)
        {
            RunManager.instance = new GameObject().AddComponent<RunManager>();
        }
        return instance;
    }
    
    [SerializeField] private GameObject escapedUI;
    [SerializeField] private GameObject caughtUI;
    [SerializeField] private GameObject gameEndUI;

    [SerializeField] private EllerMazeGenerator mazeGenerator;
    [SerializeField] private DebugMazeViewer mazeViewer;
    [SerializeField] private GameObject betweenRunCamera;
    [SerializeField] private GameObject duringRunUI;

    [SerializeField] private int totalRunsInGame;

    private int runsCompleted;
    private int runsWon;
    private int runsFailed;

    [SerializeField] private GameObject minotaurPrefab;
    [SerializeField] private GameObject playerPrefab;
    private Minotaur minotaur;
    private Player player;

    private void Awake()
    {
        RunManager.instance = this;
        
        runsCompleted = 0;
    }

    private void Start()
    {
        // StartRun();
        duringRunUI.SetActive(false);
    }

    public void StartRun()
    {
        mazeGenerator.GenerateMaze();
        mazeViewer.DrawMaze();
        SpawnMinotaur();
        SpawnPlayer();
        Spool.PickUp();
        minotaur.SetPlayer(player);
        duringRunUI.SetActive(true);
        LeaveNarrative();
        //probably do some vfx or whatnot here
        //music too
    }

    public void EndRun(bool victorious, bool removeThread)
    {
        Strand thisRunStrand = player.strand;
        RemoveMinotaur();
        RemovePlayer();
        runsCompleted++;
        if (victorious)
        {
            runsWon++;
        }
        else
        {
            runsFailed++;
        }
        duringRunUI.SetActive(false);
        if (removeThread)
        {
            RemoveThisRunStrands(thisRunStrand);
        }
        else
        {
            thisRunStrand.setColor(Color.red);
        }
        if (runsCompleted == totalRunsInGame)
        {
            EndGame(false); //because if you had won, we would be ending this through MazeMappageCalculator (for now)
        }
        else
        {
            TriggerNarrative(victorious);
        }
    }

    public void EndGame(bool victorious, bool escaped = false)
    {
        RemoveMinotaur();
        RemovePlayer();
        if (duringRunUI.activeSelf) duringRunUI.SetActive(false);
        if (victorious)
        {
            StoryConstants.currentStoryText = StoryConstants.gameEndVictorious;
        }
        else if (escaped)
        {
            StoryConstants.currentStoryText = StoryConstants.gameEndDefeatEscaped;
        }
        else
        {
            StoryConstants.currentStoryText = StoryConstants.gameEndDefeat;
        }
        gameEndUI.SetActive(true);
        betweenRunCamera.SetActive(true);
    }

    private void SpawnMinotaur()
    {
        minotaur = Instantiate(minotaurPrefab).GetComponentInChildren<Minotaur>();
    }

    private void SpawnPlayer()
    {
        player = Instantiate(playerPrefab).GetComponentInChildren<Player>();
    }
    
    private void RemoveMinotaur()
    {
        Destroy(minotaur.transform.parent.gameObject);
    }

    private void RemovePlayer()
    {
        Destroy(player.transform.parent.gameObject);
    }

    private void RemoveThisRunStrands(Strand thisRunStrand)
    {
        //TODO: Aleks do this
        // only needs to affect the logical maze, no need to worry about the visuals
        MazeNode[,] maze = LogicalMaze.getMazeArray();
        for (int x = 0; x < maze.GetLength(0); x++)
        {
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                if (maze[x,y].IsThreadNorth() && System.Object.ReferenceEquals(maze[x,y].GetThreadsNorth().Peek(), thisRunStrand))
                {
                    maze[x,y].GetThreadsNorth().Pop();
                }
                if (maze[x,y].IsThreadSouth() && System.Object.ReferenceEquals(maze[x,y].GetThreadsSouth().Peek(), thisRunStrand))
                {
                    maze[x,y].GetThreadsSouth().Pop();
                }
                if (maze[x,y].IsThreadWest() && System.Object.ReferenceEquals(maze[x,y].GetThreadsWest().Peek(), thisRunStrand))
                {
                    maze[x,y].GetThreadsWest().Pop();
                }
                if (maze[x,y].IsThreadEast() && System.Object.ReferenceEquals(maze[x,y].GetThreadsEast().Peek(), thisRunStrand))
                {
                    maze[x,y].GetThreadsEast().Pop();
                }
            }
        }
    }

    private void LeaveNarrative()
    {
        if (!caughtUI.activeSelf) caughtUI.SetActive(false);
        if (!escapedUI.activeSelf) escapedUI.SetActive(false);
        betweenRunCamera.SetActive(false);
    }
    
    private void TriggerNarrative(bool escaped)
    {
        // Update story text
        if (escaped)
        {
            StoryConstants.currentStoryText = StoryConstants.escapeTexts[runsCompleted-1];
            escapedUI.SetActive(true);
        }
        else
        {
            StoryConstants.currentStoryText = StoryConstants.caughtTexts[runsCompleted-1];
            caughtUI.SetActive(true);
        }
        betweenRunCamera.SetActive(true);
    }   
}
