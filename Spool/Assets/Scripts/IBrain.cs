using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct BrainResponse 
{
    public MazeNode nextNode;
    public float speed;
    public IBrain nextBrain;
}

public interface IBrain 
{
    // Currently this is tightly coupled to the Maze system, and only handles movement.
    // Could certainly be expanded to a more generic and encapsulated system

    public BrainResponse GetBrainResponse(MazeNode currentNode, Creature target = null);
    public string GetStateString();
}
