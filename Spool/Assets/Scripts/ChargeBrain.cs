using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeBrain : IBrain
{
    // store the last seen target position, so that we can keep charging afterwards
    private MazeNode lastSeenTargetNode;
    
    public BrainResponse GetBrainResponse(MazeNode currentNode, Creature target)
    {
        if (NavigationUtils.HasLineOfSight(currentNode, target.currentMazeNode))
        {
            lastSeenTargetNode = target.currentMazeNode;
        }

        
        BrainResponse response = new BrainResponse();
        response.speed = MinotaurSpeeds.chargeSpeed;
        if (lastSeenTargetNode != null)
        {
            //protect peeking and also make the logic a touch easier. 
            response.nextNode = NavigationUtils.GetNextNodeOnPathToPosition(currentNode, lastSeenTargetNode);
        }
        response.nextBrain = ChooseNextBrain(currentNode, target);

        return response;
    }

    private IBrain ChooseNextBrain(MazeNode currentNode, Creature target)
    {
        if (NavigationUtils.HasLineOfSight(currentNode, target.currentMazeNode)
            || (lastSeenTargetNode != null && currentNode != lastSeenTargetNode)) //there is a last node, and we're not there yet
        {
            return this;
        }
        else
        {
            lastSeenTargetNode = null;
            return BrainBank.wanderingBrain;
        }
    }
    
    public string GetStateString()
    {
        return "State: Charging\nTarget: " + lastSeenTargetNode?.xCoord + " " + lastSeenTargetNode?.yCoord;
    }
}
