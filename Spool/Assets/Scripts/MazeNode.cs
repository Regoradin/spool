using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeNode
{
    public int xCoord { get; }
    public int yCoord { get; }

    private MazeNode[] connectedNodes;
    private List<Creature> currentCreatures;

    private bool isDoor; // might not actually need this?

    // if ThreadDIR is true, there is a thread from the center of that cell, in that direction
    private Stack<Strand> threadNorth;
    private Stack<Strand> threadSouth;
    private Stack<Strand> threadEast;
    private Stack<Strand> threadWest;

    public MazeNode(int xCoord,
                    int yCoord,
                    MazeNode north = null,
                    MazeNode south = null,
                    MazeNode east = null,
                    MazeNode west = null,
                    bool isDoor = false)
    {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        connectedNodes = new MazeNode[] { north, east, south, west };
        this.isDoor = isDoor;

        currentCreatures = new List<Creature>();
        threadNorth = new Stack<Strand>();
        threadSouth = new Stack<Strand>();
        threadEast = new Stack<Strand>();
        threadWest = new Stack<Strand>();
    }

    public void Connect(MazeNode north = null,
                        MazeNode south = null,
                        MazeNode east = null,
                        MazeNode west = null,
                        bool isDoor = false)
    {
        // this is for after-creation connecting of nodes
        connectedNodes[0] = north ?? connectedNodes[0];
        connectedNodes[1] = east ?? connectedNodes[1];
        connectedNodes[2] = south ?? connectedNodes[2];
        connectedNodes[3] = west ?? connectedNodes[3];
    }

    public void Disconnect(bool north = false,
			   bool south = false,
			   bool east = false,
			   bool west = false)
	
    {
	//sorta janky
	connectedNodes[0] = north ? null : connectedNodes[0];
	connectedNodes[1] = east ? null : connectedNodes[1];
	connectedNodes[2] = south ? null : connectedNodes[2];
	connectedNodes[3] = west ? null : connectedNodes[3];
    }

    public void Enter(Creature creature)
    {
        currentCreatures.Add(creature);
    }

    public void Exit(Creature creature)
    {
        if (currentCreatures.Contains(creature))
        {
            currentCreatures.Remove(creature);
        }
        else
        {
            Debug.LogWarning(string.Format("Tried to remove {0} from MazeNode at {1},{2} that is not in that node!",
                                           creature, xCoord, yCoord));
        }
    }

    public List<Creature> GetCurrentCreatures()
    {
	return currentCreatures;
    }

    public bool IsDoor()
    {
	return isDoor;
    }

    // These all will return null if there is not a connected node in that direction (i.e. a wall or the edge of the map)
    public MazeNode[] GetConnectedNodes()
    {
        return connectedNodes;
    }
    public MazeNode GetNorthDoor()
    {
        return connectedNodes[0];
    }
    public MazeNode GetEastDoor()
    {
        return connectedNodes[1];
    }
    public MazeNode GetSouthDoor()
    {
        return connectedNodes[2];
    }
    public MazeNode GetWestDoor()
    {
        return connectedNodes[3];
    }

    // Getters for thread information
    public bool HasAnyThread()
    {
	return (IsThreadNorth() || IsThreadSouth() || IsThreadEast() || IsThreadWest());
    }
    public bool[] GetThreadArray()
    {
        return new bool[] { IsThreadNorth(), IsThreadEast(), IsThreadSouth(), IsThreadWest() };
    }
    public bool IsThreadNorth()
    {
        return threadNorth.Count > 0;
    }
    public bool IsThreadSouth()
    {
        return threadSouth.Count > 0;
    }
    public bool IsThreadEast()
    {
        return threadEast.Count > 0;
    }
    public bool IsThreadWest()
    {
        return threadWest.Count > 0;
    }

    // Getters for the whole stack
    public Stack<Strand> GetThreadsNorth()
    {
        return this.threadNorth;
    }

    public Stack<Strand> GetThreadsSouth()
    {
        return this.threadSouth;
    }

    public Stack<Strand> GetThreadsEast()
    {
        return this.threadEast;
    }

    public Stack<Strand> GetThreadsWest()
    {
        return this.threadWest;
    }

    // Lays down a thread, only if it the top strand doesn't match the strand being placed
    public void SetThreadNorth(Strand strand)
    {
        if (this.threadNorth.Count == 0 || (!System.Object.ReferenceEquals(this.threadNorth.Peek(), strand)))
        {
            this.threadNorth.Push(strand);
        }   
    }
    public void SetThreadSouth(Strand strand)
    {
        if (this.threadSouth.Count == 0 || (!System.Object.ReferenceEquals(this.threadSouth.Peek(), strand)))
        {
            this.threadSouth.Push(strand);
        }   
    }
    public void SetThreadEast(Strand strand)
    {
        if (this.threadEast.Count == 0 || (!System.Object.ReferenceEquals(this.threadEast.Peek(), strand)))
        {
            this.threadEast.Push(strand);
        }
    }
    public void SetThreadWest(Strand strand)
    {
        if (this.threadWest.Count == 0 || (!System.Object.ReferenceEquals(this.threadWest.Peek(), strand)))
        {
            this.threadWest.Push(strand);
        }   
    }
}
