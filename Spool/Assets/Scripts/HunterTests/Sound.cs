using UnityEngine.Audio;
using System;
using UnityEngine;

[Serializable]
public class Sound
{
    [Tooltip("Name to be called with Play() method")]
    public string name;

    public AudioClip clip;

    [Range (0f, 1f), Tooltip("default: 1")]
    public float volume = 1; 
    
    [Range (0f, 2f), Tooltip("default: 1")]
    public float pitch = 1;

    public bool usesReverb, isLooping, isMusic;

    [HideInInspector]
    public AudioSource source;
}
