using UnityEngine.Audio;
using System;
using UnityEngine;

// call:
// FindObjectOfType<AudioManager>().Play("SoundName");
// to activate the named sound
public class AudioManager : MonoBehaviour
{
    [Tooltip("The list of all sounds to be used by the Audio Manager. Each sound will have an AudioSource component created according to it's given specificaions")]
    public Sound[] sounds;

    public static AudioManager instance;

    private Sound currentSound;
    
    void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound sound in sounds) 
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;

            sound.source.volume = sound.volume;
            sound.source.bypassEffects = !sound.usesReverb;
            sound.source.loop = sound.isLooping;
        }
        AudioReverbFilter reverb = gameObject.AddComponent<AudioReverbFilter>();
        reverb.reverbPreset = AudioReverbPreset.Stoneroom;
    }

    void Start () 
    {
        Play("Music_Menu");
    }

    public void Play (string soundName) 
    {
        Sound s = Array.Find(sounds, sound => sound.name == soundName);
        s.source.Play();
        if (s.isMusic)    
            currentSound = s;
    }

    public void StopCurrentSound () 
    {
        currentSound.source.Stop();
    }
}
