using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    [SerializeField] private Transform sprite;
    [SerializeField] private PlayerInputManager inputManager;
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource audioSource;

    [SerializeField, Range (0f, 2f)] private float moveDuration = 0.5f;
    [SerializeField, Range (0f, 2f)] private float animatorSpeed = 1f;
    [SerializeField] private float yOffset = 1;

    const string LeftUp = "TheseusLeftUp";
    const string RightDown = "TheseusDownRight";
    const string Thread_LeftUp = "TheseusThreadLeftUp";
    const string Thread_RightDown = "TheseusThreadDownRight";
    const string Idle = "TheseusIdle";
    const string Thread_Idle = "TheseusThreadIdle";

    const string MinoLeft = "MinotaurLeft";
    const string MinoRight = "MinotaurRight";
    const string MinoUp = "MinotaurUp";
    const string MinoDown = "MinotaurDown";

    private float elapsedTime;

    private Vector3 startingPosition, endingPosition;

    private bool isRunning;
    
    void Start ()
    {
        animator.speed = animatorSpeed;
    }

    void Update () 
    {
        elapsedTime += Time.deltaTime;
        float percentageComplete = elapsedTime / moveDuration;
        sprite.position = Vector3.Lerp(startingPosition, endingPosition, percentageComplete);

    }
    
    public void MoveSprite (MazeNode first, MazeNode second, bool isPlayer, bool hasThread)
    {
        if (first == null)
        {
            // this handles the very first "move", which is from null to the starting space
            first = second;
        }
        if(first != null && second != null)
        {
            startingPosition =  new Vector3(first.xCoord, yOffset, -first.yCoord);
            endingPosition = new Vector3(second.xCoord, yOffset, -second.yCoord);
            if (isPlayer)
            {
                AnimatePlayer(second.xCoord - first.xCoord, second.yCoord - first.yCoord, hasThread);
            } 
            else 
            {
                AnimateMinotaur(second.xCoord - first.xCoord, second.yCoord - first.yCoord);
            }
            elapsedTime = 0;
        }
    
    }

    public float GetMoveDuration ()
    {
        return moveDuration;
    }

    public void SetMoveDuration(float duration)
    {
        // only used for changing the minotaur speed
        moveDuration = duration;
    }

    public void HasMoved () 
    {
        inputManager.HasMoved();
    }

    private void AnimatePlayer (int xDiff, int yDiff, bool hasThread)
    {
        if (xDiff == 1 || yDiff == 1)
        {
            if(hasThread)
                animator.Play(Thread_LeftUp);
            else
                animator.Play(LeftUp);
        }
        else if (xDiff == -1 || yDiff == -1)
        {
            if(hasThread)
                animator.Play(Thread_RightDown);
            else
                animator.Play(RightDown);
        }
    }

    private void AnimateMinotaur (int xDiff, int yDiff)
    {
        audioSource.Play();
        if (xDiff == -1)
        {
            animator.Play(MinoRight);
        }
        else if (xDiff == 1)
        {
            animator.Play(MinoLeft);
        }
        else if (yDiff == -1)
        {
            animator.Play(MinoUp);
        }
        else if (yDiff == 1)
        {
            animator.Play(MinoDown);
        }
    }

    
}
