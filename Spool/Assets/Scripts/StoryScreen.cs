using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;

public class StoryScreen : MonoBehaviour
{
    [SerializeField] private GameObject nextUI;
    [SerializeField] private bool startsRun;
    [SerializeField] private float clickDelay = StoryConstants.clickDelayOnNarrativeScreens;
    [SerializeField] private bool textVaries;
    [SerializeField] private TMP_Text text;
    [SerializeField] private bool startOn = false;
    [SerializeField] private bool returnToStart = false;
    private float timeSinceStart;

    private void Awake()
    {
        Debug.LogWarning("UI set to false");
        if (!startOn) this.gameObject.SetActive(false);
        timeSinceStart = 0f;
        if (textVaries) text.text = StoryConstants.currentStoryText;
    }

    private void Start()
    {
        timeSinceStart = 0f;
        if (textVaries) text.text = StoryConstants.currentStoryText;
    }

    private void OnEnable()
    {
        timeSinceStart = 0f;
        if (textVaries) text.text = StoryConstants.currentStoryText;
    }
    
    void Update()
    {
        timeSinceStart += Time.deltaTime;
        if (Input.GetMouseButton(0) && timeSinceStart > clickDelay)
        {
            Debug.LogWarning("pressssed");
            if (startsRun) {
                RunManager.GetInstance().StartRun();
                Debug.LogWarning("would be starting the run right now!");
            }
            else if (returnToStart)
            {
                SceneManager.LoadScene("Start");
            } 
            else
            {
                Debug.LogWarning("did this instead");
                nextUI.SetActive(true);
            }
            this.gameObject.SetActive(false);
        }
    }
}