using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(DebugCreatureViewer))]
public class Minotaur : MonoBehaviour
{
    [SerializeField]
    private PlayerMover mover;
    private Creature creature;
    [SerializeField] private float movementDelay;

    private Creature playerCreature;

    private IBrain movementBrain = BrainBank.wanderingBrain;

    private void Awake()
    {
        creature = new Creature("Minotaur", 12, 12, false, null, mover);        
        GetComponent<DebugCreatureViewer>().SetCreature(creature);
        StartCoroutine(MovementLoop());
    }


    public void SetPlayer(Player player)
    {
        this.playerCreature = player.creature;
    }

    private IEnumerator MovementLoop()
    {
        BrainResponse brainResponse = movementBrain.GetBrainResponse(creature.currentMazeNode, playerCreature);
        
        MazeNode nextRoom = brainResponse.nextNode;
        Vector2Int move = DirectionToVector(System.Array.IndexOf(creature.GetAvailableRooms(), nextRoom));
        
        float delay = 1 / brainResponse.speed;

        creature.TryMove(move, delay);

        movementBrain = brainResponse.nextBrain;
        
        yield return new WaitForSeconds(delay);
        StartCoroutine(MovementLoop());
    }

    
    private Vector2Int DirectionToVector(int direction)
    {
        switch(direction)
        {
            case 0:
                return new Vector2Int(0, -1);
            case 1:
                return new Vector2Int(1, 0);
            case 2:
                return new Vector2Int(0, 1);
            case 3:
                return new Vector2Int(-1, 0);
            default:
                return Vector2Int.zero;
        }
    }
}
