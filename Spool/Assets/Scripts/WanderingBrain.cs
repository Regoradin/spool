using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingBrain : IBrain
{
    private Queue<MazeNode> recentlyVisited;

    private Vector2Int destination;

    public WanderingBrain()
    {
        recentlyVisited = new Queue<MazeNode>();
    }

    public BrainResponse GetBrainResponse(MazeNode currentNode, Creature target = null)
    {
        BrainResponse response = new BrainResponse();

        response.speed = MinotaurSpeeds.wanderSpeed;
        //response.nextNode = ChooseRandomConnectedNode(currentNode);
        response.nextNode = GoToRandomDestination(currentNode);
        response.nextBrain = ChooseNextBrain(currentNode, target);

        recentlyVisited.Enqueue(response.nextNode);
        if (recentlyVisited.Count > 10)
        {
            recentlyVisited.Dequeue();
        }

        return response;
    }

    private MazeNode GoToRandomDestination(MazeNode currentNode)
    {
        if ((currentNode.xCoord == destination.x && currentNode.yCoord == destination.y)
            || destination == Vector2Int.zero)
        {
            destination = new Vector2Int(Random.Range(0, 15), Random.Range(0, 15));
        }
        return NavigationUtils.GetNextNodeOnPathToPosition(currentNode,
                                                           LogicalMaze.getMazeNode(destination.x, destination.y));
    }

    private MazeNode ChooseRandomConnectedNode(MazeNode currentNode)
    {
        List<MazeNode> candidates = new List<MazeNode>();

        foreach(MazeNode node in currentNode.GetConnectedNodes())
        {
            if (recentlyVisited.Contains(node))
            {
                candidates.Add(node);                
            }
            else
            {
                // very messy way to bias towards new places
                candidates.Add(node);
                candidates.Add(node);
                candidates.Add(node);
                candidates.Add(node);
            }
        }

        MazeNode nextNode = candidates[Random.Range(0, candidates.Count)];


        return nextNode;
    }

    private IBrain ChooseNextBrain(MazeNode currentNode, Creature target)
    {
        if (target != null && NavigationUtils.HasLineOfSight(currentNode, target.currentMazeNode))
        {
            return BrainBank.chargeBrain;
        }
        if (currentNode.HasAnyThread()) //TODO: this should be the next node
        {
            return BrainBank.threadBrain;
        }
        return this;
    }

    public string GetStateString()
    {
        if (Random.Range(0f, 1f) > 0.8f)
        {
            return "State: Wandering happily";
        }
        return "State: Wandering";
    }
}
