using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerInput))]
public class PlayerInputManager : MonoBehaviour
{
    [SerializeField]
    private PlayerMover mover;
    
    private Creature creature;

    private Vector2Int moveInputDirection;

    private float elapsedTime, moveDelay;
    private bool canMove = true;

    private Vector2Int movement;
    
    void Start () 
    {
        moveDelay = mover.GetMoveDuration();
    }
    
    void Update () 
    {
        elapsedTime += Time.deltaTime;
        if (canMove)
        {
            creature.TryMove(movement);
        }
        if (elapsedTime >= moveDelay)
        {
            canMove = true;
        }
    }
    
    // Event called anytime there is a recognized Move input
    private void OnMove (InputValue input) 
    {
        moveInputDirection = ClampMoveInput(input.Get<Vector2>());
        moveInputDirection *= new Vector2Int(1, -1);
        movement = moveInputDirection;
    }

    private void OnInteract()
    {
        creature.InteractWithMazeNode();
    }

    // Clamps and converts the input Vector2 to Vector2Int
    private Vector2Int ClampMoveInput (Vector2 rawInput)
    {
        if (Mathf.Abs(rawInput.x) > 0 && Mathf.Abs(rawInput.y) > 0)
        {
            rawInput.x = 0;
            rawInput.y = 0;
        }
        return new Vector2Int ((int) rawInput.x, (int) rawInput.y);

    }

    public Creature Creature
    {
        get { return creature; }
        set { creature = value; }
    }

    public void HasMoved () 
    {
        canMove = false;
        elapsedTime = 0;
    }
}
