using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StoryConstants
{
    public static float clickDelayOnNarrativeScreens = 2f;
    public static string[] escapeTexts = new string[] {
        "Echoing through the labyrinth you hear: ‘I know that scent… Why have you returned?’",
        "Echoing through the labyrinth you hear: 'What are you doing here little hero? Back to finish what you started?'",   
        "Echoing through the labyrinth you hear: 'Do you really want to go through this again, little hero?'",
        "Echoing through the labyrinth you hear: 'You ran away once. What makes you so confident you won’t do it again?'"
    };

    public static string[] caughtTexts = new string[] {
        "The Minotaur: 'You… Why have you returned? Leave at once!' The Minotaur chases you out of the labyrinth, you’ll have to return tomorrow.",
        "The Minotaur: 'You dare you come here after what you did to me? I will never forgive you.' The Minotaur chases you out of the labyrinth, you’ll have to return tomorrow.",
        "The Minotaur: 'I’m tired, little hero. I don’t want to go through this again. Please just leave.' The Minotaur chases you out of the labyrinth, you’ll have to return tomorrow.",
        "The Minotaur: 'Why do you keep coming back? Don’t you realize this is futile?' The Minotaur chases you out of the labyrinth, you’ll have to return tomorrow.",
        "The Minotaur: 'It’s over little hero. You can never hurt me again. Just leave me at last.'"
    };

    public static string gameEndVictorious = "The Minotaur: 'Why are you here little hero? What do you really want?'\n\nTheseus: 'I came to fix the biggest mistake I ever made. I’m sorry. I never should have left you here.'\n\nThe Minotaur: 'Theseus…'\n\nTheseus: 'I love you.'\n\nThe Minotaur: 'I… love you too.'\n\nWith the thread spread through the Labyrinth, you and the Minotaur may come and go as you please, together.";
    
    public static string gameEndDefeat = "The Minotaur: 'It’s over little hero. You can never hurt me again. Just leave me at last.'\n\nAs you exit you hear the door seal behind you.\n\nThere is nothing left for you here.";

    public static string gameEndDefeatEscaped = "Echoing through the labyrinth you hear: 'Run away little hero, tell them all you slayed me if you wish, just leave me alone at last.'\n\nAs you exit you hear the door seal behind you.\n\nThere is nothing left for you here.";

    public static string currentStoryText;
}