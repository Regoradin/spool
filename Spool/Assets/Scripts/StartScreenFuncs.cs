using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartScreenFuncs : MonoBehaviour
{
    public GameObject saturatedLogo;
    public float fadeSpeed;
    private Image saturatedLogoRenderer;
    
    [SerializeField] private AudioManager audioManager;

    public GameObject credits;

    private void Awake()
    {
        // saturatedLogoRenderer = saturatedLogo.GetComponent<Image>();
        // saturatedLogoRenderer.color = new Color(1f, 1f, 1f, 0f);

        credits.SetActive(false);
    }
        
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void ActivateCredits()
    {
        credits.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            credits.SetActive(false);
        }
    }

    public void DoTheGameStartThing()
    {
        audioManager.StopCurrentSound();
        int songNum = Random.Range(1, 3);
        if(songNum == 1)
            audioManager.Play("Music_Ambient");
        else
            audioManager.Play("Music_Prelude");
        LoadScene("Game");
        // StartCoroutine(FadeInAndStart(0f));
    }

    public void Quit()
    {
        Application.Quit();
    }

    private IEnumerator FadeInAndStart(float alpha)
    {
        alpha += fadeSpeed * Time.deltaTime;

        saturatedLogoRenderer.color = new Color(1f, 1f, 1f, alpha);
        if (alpha >= 1f)
        {
            LoadScene("Game");
        }
        else
        {
            yield return null;
            StartCoroutine(FadeInAndStart(alpha));
        }
    }
}
