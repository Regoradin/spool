using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MinotaurSpeeds
{
    public static float wanderSpeed = 4f;
    public static float threadSpeed = 3.2f;
    public static float chargeSpeed = 6f;
}
