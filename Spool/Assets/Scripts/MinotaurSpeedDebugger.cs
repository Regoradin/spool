using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MinotaurSpeedDebugger : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI wanderText;
    [SerializeField] private TextMeshProUGUI chargeText;
    [SerializeField] private TextMeshProUGUI threadText;
    
    public void SetMinotaurWanderSpeed(float v)
    {
        MinotaurSpeeds.wanderSpeed = v;
    }
    public void SetMinotaurChargeSpeed(float v)
    {
        MinotaurSpeeds.chargeSpeed = v;
    }
    public void SetMinotaurThreadSpeed(float v)
    {
        MinotaurSpeeds.threadSpeed = v;
    }

    private void Update()
    {
        wanderText.text = "Wander: " + MinotaurSpeeds.wanderSpeed;
        chargeText.text = "Charge: " + MinotaurSpeeds.chargeSpeed;
        threadText.text = "Thread: " + MinotaurSpeeds.threadSpeed;
    }
    
}
