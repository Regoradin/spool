using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCreatureViewer : MonoBehaviour
{
    //private Transform sphere;
    [SerializeField] 
    private Transform player;
    private Creature creature;
    [SerializeField] private Color color;

    [SerializeField] private Transform creatureCamera;

    private void Awake()
    {
        if (creatureCamera != null)
        {

            //set this position ahead of time in order for the camera to set it's offset correctly
            //sphere.position = new Vector3(creature.GetCurrentPosition().x, 1, -creature.GetCurrentPosition().y);
            creatureCamera.GetComponent<CameraSmoothFollow>().SetTarget(player.transform);}
    }

    public void SetCreature(Creature creature)
    {
        this.creature = creature;
    }
    
    private void OnDestroy()
    {
        if (creatureCamera != null)
        {
            //because the smooth follow un-parents the creature camera
            Destroy(creatureCamera.gameObject);
        }
    }
}
