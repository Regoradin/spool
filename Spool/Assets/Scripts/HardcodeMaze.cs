using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardcodeMaze : MonoBehaviour
{
    // This will create a new hardcoded maze on Awake
    // Possibly consider as inspiration for the actual maze generator?

    private void Awake()
    {
        Strand strand = new Strand(1, Color.black);
        MazeNode[,] maze = new MazeNode[5, 3];

        maze[0, 0] = new MazeNode(0, 0);
        maze[0, 0].SetThreadNorth(strand);
        maze[1, 0] = new MazeNode(1, 0);
        maze[1, 0].SetThreadSouth(strand);
        maze[2, 0] = new MazeNode(2, 0);
        maze[2, 0].SetThreadEast(strand);
        maze[3, 0] = new MazeNode(3, 0);
        maze[3, 0].SetThreadWest(strand);
        maze[3, 0].SetThreadSouth(strand);
        maze[4, 0] = new MazeNode(4, 0);
        maze[0, 1] = new MazeNode(0, 1);
        maze[1, 1] = new MazeNode(1, 1);
        maze[2, 1] = new MazeNode(2, 1);
        maze[2, 1].SetThreadNorth(strand);
        maze[2, 1].SetThreadSouth(strand);
        maze[2, 1].SetThreadWest(strand);
        maze[2, 1].SetThreadEast(strand);
        maze[3, 1] = new MazeNode(3, 1);
        maze[3, 1].SetThreadWest(strand);
        maze[3, 1].SetThreadNorth(strand);
        maze[3, 2] = new MazeNode(3, 2);

        maze[0, 0].Connect(east: maze[1, 0]);
        maze[1, 0].Connect(west: maze[0, 0], east: maze[2, 0]);
        maze[2, 0].Connect(west: maze[1, 0], east: maze[3, 0]);
        maze[3, 0].Connect(west: maze[2, 0], east: maze[4, 0], south: maze[3, 1]);
        maze[4, 0].Connect(west: maze[3,0 ]);
        maze[0, 1].Connect(east: maze[1, 1]);
        maze[1, 1].Connect(west: maze[0, 1], east: maze[2, 1]);
        maze[2, 1].Connect(west: maze[1, 1], east: maze[3, 1]);
        maze[3, 1].Connect(west: maze[2, 1], north: maze[3, 0], south: maze[3, 2]);
        maze[3, 2].Connect(north: maze[3, 1]);

        LogicalMaze.CreateMaze(maze, maze[1,0]);
    }
}
