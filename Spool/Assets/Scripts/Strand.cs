using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strand
{
    private int round;
    private Color color;

    public Strand(int round, Color color)
    {
        this.round = round;
        this.color = color;
    }

    public Color getColor()
    {
        return this.color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }
}