using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;

public static class NavigationUtils
{
    public static bool HasLineOfSight(MazeNode first, MazeNode second)
    {
        // Currently checks if they can see each other in straight axis lines.
        // no angles
        if (first.xCoord == second.xCoord
            && first.yCoord == second.yCoord)
        {
            // they are the same
            return true;
        }
        
        int direction = -1;
        if (first.xCoord == second.xCoord)
        {
            if (first.yCoord > second.yCoord)
            {
                direction = 0;
            }
            else
            {
                direction = 2;
            }
        }
        if (first.yCoord == second.yCoord)
        {
            if (first.xCoord > second.xCoord)
            {
                direction = 3;
            }
            else
            {
                direction = 1;
            }
        }
        if (direction == -1)
        {
            // the two nodes aren't aligned on an axis
            return false;
        }

        MazeNode currentNode = first;
        while (currentNode != null)
        {
            if (currentNode == second)
            {
                // we were able to find the second node
                return true;
            }
            currentNode = currentNode.GetConnectedNodes()[direction];
        }
        
        return false;
    }

    public static MazeNode GetNextNodeOnPathToPosition(MazeNode fromNode, MazeNode targetNode)
    {
        if (targetNode.xCoord == fromNode.xCoord && targetNode.yCoord == fromNode.yCoord)
        {
            return fromNode;
        }
        //A* (or really Dijkstra's until anyone can be bothered to add a heuristic)

        SimplePriorityQueue<MazeNode, float> fringe = new SimplePriorityQueue<MazeNode, float>();
        Dictionary<MazeNode, MazeNode> previousNode = new Dictionary<MazeNode, MazeNode>();
        Dictionary<MazeNode, float> nodeCosts = new Dictionary<MazeNode, float>();

        fringe.Enqueue(fromNode, 1);
        nodeCosts[fromNode] = 1f;

        MazeNode headMazeNode = fromNode;
        
        while (!(headMazeNode.xCoord == targetNode.xCoord && headMazeNode.yCoord == targetNode.yCoord))
        {
            foreach(MazeNode node in headMazeNode.GetConnectedNodes())
            {
                if (node != null
                    && !nodeCosts.ContainsKey(node)) // filters out previously explored
                {
                    float nodeCost = nodeCosts[headMazeNode] + 1; // NOTE: all nodes are the same distance here
                    nodeCosts[node] = nodeCost;
                    previousNode[node] = headMazeNode;
                    fringe.Enqueue(node, nodeCost);
                }
            }

            if (fringe.Count != 0)
            {
                // if this is not the case, probably the round is over and we don't need this to work anymore
                // otherwise... bad things are probably happening
                headMazeNode = fringe.Dequeue();                
            }
        }
        // Unpack the path until we get to the next step
        // no need to keep the whole path around, because we're just planning the next step
        // cacheing? who needs that...
        while (previousNode[headMazeNode] != fromNode)
        {
            headMazeNode = previousNode[headMazeNode];
        }

        return headMazeNode;
    }

}
