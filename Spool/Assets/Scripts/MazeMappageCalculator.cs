using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MazeMappageCalculator : MonoBehaviour
{
    public float mappagePercent { get; private set;}
    [Range(0, 1)]
    public float mapPercentToWin = 1f;

    public Slider slider;

    private void Update()
    {
        if (LogicalMaze.getMazeArray() == null) return;
        int mappedNodes = 0;
        int totalNodes = 0;

        MazeNode[,] maze = LogicalMaze.getMazeArray();

        for(int x = 0; x < maze.GetLength(0); x++)
        {
            for(int y = 0; y < maze.GetLength(1); y++)
            {
                if (maze[x,y].HasAnyThread())
                {
                    mappedNodes++;
                }
                totalNodes++;
            }
        }

        if (mappedNodes == totalNodes)
        {
            Debug.LogWarning("Ending game!");
            RunManager.GetInstance().EndGame(true);
        }

        float mappedPercent = (float) mappedNodes / (float) totalNodes;
        
        mappagePercent = mappedPercent / mapPercentToWin;

        if (slider)
        {
            slider.value = mappagePercent;
        }
    }
}
