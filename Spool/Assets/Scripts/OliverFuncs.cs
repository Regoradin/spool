using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliverFuncs : MonoBehaviour
{
    public EllerMazeGenerator mazeGen;

    public void GenerateMaze()
    {
        mazeGen.GenerateMaze();
    }
}
