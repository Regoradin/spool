using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LogicalMaze
{
    private static MazeNode doorNode;
    // array representation of the Maze arranged [x, y]
    // [0, 0] is the top left, increasing down and to the right
    private static MazeNode[,] maze;
        
    private static void ClearMaze()
    {
        // eventually this (or something similar) will have to handle not clearing the tiles with thread
        maze = new MazeNode[maze.GetLength(0), maze.GetLength(1)];
        doorNode = null;
    }

    public static void CreateMaze(MazeNode[,] maze, MazeNode firstNode)
    {
        // very simple sorta-constructor for taking a handmade maze
        LogicalMaze.maze = maze;
        LogicalMaze.doorNode = firstNode;
    }

    public static MazeNode[,] getMazeArray()
    {
        return maze;
    }

    public static MazeNode getMazeNode(int x, int y)
    {
        if (x >= 0
            && y >= 0
            && x < maze.GetLength(0)
            && y < maze.GetLength(1))           
        {
            return maze[x, y];
        }
        return null;
    }

    public static MazeNode getDoorNode()
    {
	return doorNode;
    }

    public static bool IsConnected(MazeNode first, MazeNode second)
    {
	return (System.Array.IndexOf(first.GetConnectedNodes(), second) != -1)
	    && (System.Array.IndexOf(second.GetConnectedNodes(), first) != -1);
    }
    

}
