using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSmoothFollow : MonoBehaviour
{
    private Transform target;
    private Vector3 offset;

    public float smoothTime;
    private Vector3 velocity;

    public void SetTarget(Transform target)
    {
        this.target = target;
        offset = transform.localPosition;
        transform.SetParent(null);
        transform.position = target.position + offset;
    }

    private void Update()
    {
        if (target != null)
        {
            Vector3 targetPosition = target.position + offset;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }
    }
}
