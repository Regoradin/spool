using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Spool
{
    public static bool isDropped {get; private set; }
    public static MazeNode node {get; private set; }

    public static void PickUp()
    {
        node = null;
        isDropped = false;
    }

    public static void Drop(MazeNode node)
    {
        isDropped = true;
        Spool.node = node;
    }
}
