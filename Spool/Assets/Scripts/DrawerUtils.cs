using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DrawerUtils
{
    public static Vector3 GetWorldPosition(int x, int y)
    {
        // Converts logical maze x and y into world coords
        //Unity is backwards of what I though, hence the negative y
        return new Vector3(x, 0, -y);
    }

}
