using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpoolDrawer : MonoBehaviour
{
    [SerializeField] private GameObject spoolPrefab;
    private GameObject spool;

    private void Awake()
    {
        spool = Instantiate(spoolPrefab);
    }

    private void Update()
    {
        spool.SetActive(Spool.isDropped);
        if (Spool.node != null)
        {
            spool.transform.position = DrawerUtils.GetWorldPosition(Spool.node.xCoord, Spool.node.yCoord);
        }
    }
}
